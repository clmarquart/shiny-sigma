
#' @import htmlwidgets
#' @export
sigma <- function(gexf, drawEdges = TRUE, drawNodes = TRUE,
    width = NULL, height = NULL, type = "matrix", name = "sig",
    clickNode = NULL, clickEdge = NULL, doubleClickNode = NULL,
    overNode = NULL, outNode = NULL, overEdge = NULL, outEdge = NULL
  ) {

  #print(gexf);
  if(is.null(gexf)) {
    return(NULL);
  }

  # read the gexf file
  if(is.character(gexf)) {
    data = rjson::fromJSON(gexf)
  } else if(is.list(gexf) ) {
    data = gexf;
  } else if(is.data.frame(gexf)) {
    data = as.matrix(gexf);
  } else {
    data <- paste(readLines(gexf), collapse="\n")
  }

  # create a list that contains the settings
  settings <- list(
    drawEdges = drawEdges,
    drawNodes = drawNodes,
    type = type,
    name = name,
    events = list(
      clickNode = clickNode,
      clickEdge = clickEdge,
      doubleClickNode = doubleClickNode,
      overNode = overNode,
      outNode = outNode,
      overEdge = overEdge,
      outEdge = outEdge
    )
  )

  # pass the data and settings using 'x'
  x <- list(
    data = data,
    settings = settings
  )

  # create the widget
  htmlwidgets::createWidget("sigma", x, width = width, height = height)
}

#' @export
sigmaOutput <- function(outputId, width="100%", height="400px", name="sig",
                        nodeClick=NULL, inline=F
) {
  shinyWidgetOutput(outputId, "sigma", width, height, package = "sigma", inline = inline);
}

#' @export
renderSigma <- function(expr, env = parent.frame(), quoted = FALSE) {
  if (!quoted) { expr <- substitute(expr) } # force quoted
  shinyRenderWidget(expr, sigmaOutput, env, quoted = TRUE)
}
