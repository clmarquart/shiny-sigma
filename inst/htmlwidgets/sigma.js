window.sigmaGraphs = [];
HTMLWidgets.widget({

  name: "sigma",

  type: "output",

  factory: function(el, width, height) {
    
    var sig = new sigma({
      renderers: [{
        container: document.getElementById(el.id),
        type: 'svg'
      }],
      settings : {
        backgroundColor: "#FFFFFF",
        sideMargin: 0,
        animationsTime: 500,
        zoomMin: 1,
        zoomMax: 1,
        enableCamera: false,
        minNodeSize: 0.1,
        maxNodeSize: 5,
        
        // Edge settings
        minEdgeSize: 0,
        maxEdgeSize: 10,
        batchEdgesDrawing: true,
        webglEdgesBatchSize: 1,
        edgeAnimation: "outsideIn",
        
        // Label settings
        labelThreshold: 1,
        defaultLabelSize: 10
      }
    });
    sigmaGraphs.push(sig);

    return {
      renderValue: function(x) {
        if(x.settings.events) {
          for(var evt in x.settings.events) {
            if(x.settings.events[evt] && !sig.getEvent("click").target._handlers[evt]) {
              sig.bind(evt, x.settings.events[evt]);
            }
          }
        }
        
        var data;
        if(! x.data instanceof Array) {
          // parse gexf data
          var parser = new DOMParser();
          data = parser.parseFromString(x.data, "application/xml");
        } else {
          data = x.data;
        }

        // apply settings
        x.settings.id = sig.id;
        for (var name in x.settings)
          sig.settings(name, x.settings[name]);

        if(x.settings.type == "xml") {
          // update the sigma object
          sigma.parsers.gexf(
            data,          // parsed gexf data
            sig,           // sigma object
            function() {
              // need to call refresh to reflect new settings and data
              sig.refresh();
            }
          );
        } else {
          sig.graph.clear()
          
          var foundCustom=false;
          
          if(data.axisBounds) {
            sig.settings('bounds',{
              minX: -data.axisBounds[0],
              maxX: data.axisBounds[0],
              minY: -data.axisBounds[0],
              maxY: data.axisBounds[0],
              sizeMax: 1,
              weightMax: -Infinity
            });
            sig.addAxisLines({color: "#CCCCCC"}); //data.axisBounds);
            sig.refresh();
          }
          var toAnimate = [];
          data.nodes.forEach(function(n,i) {
            if(!!n.image) foundCustom = true;
            if(n.from!=="NA" && (n.tox || n.toy)) toAnimate.push(n.id)
            sig.graph.addNode(n);
          });
          if(foundCustom) {
            CustomShapes.init(sig);
          }
          sig.refresh();
          //data.edges.forEach(function(n,i) {
          //  console.log(n);
          //  sig.graph.addEdge(n);
          //});
          var keys = Object.keys(data.edges); 
          for(var i in keys) { 
            var e = data.edges[keys[i]];
            if(!e || e.length != 7) continue; 
            
            sig.graph.addEdge({ 
              id: e[0], label: e[1], 
              size: e[2], color: e[3],
              source: e[4], target: e[5],
              type: e[6]
            }) 
          }
          sig.refresh();
          sigma.plugins.animateEdges(sig, sig.graph.edges(),sig.settings);
          if(toAnimate.length > 0) {
            //sigma.plugins.animate(sig,{x: 'tox', y:'toy' }, { nodes: toAnimate } );
            sigma.plugins.animate(
              sig,
              {x: 'tox', y:'toy' }, 
              { nodes: toAnimate } // sigmaGraphs[0].graph.nodes().filter(n=>(n.from!=="NA"&&n.tox&&n.toy)).map(n=>n.id) }
            );
          }
        }
      }

      ,resize: function(width, height) {
        for (var name in sig.renderers)
          sig.renderers[name].resize(width, height);
      }
      
      // Make the sigma object available as a property on the widget
      // instance we're returning from factory(). This is generally a
      // good idea for extensibility--it helps users of this widget
      // interact directly with sigma, if needed.
      ,s: sig
      ,graphs: sigmaGraphs
    };
  }
});
