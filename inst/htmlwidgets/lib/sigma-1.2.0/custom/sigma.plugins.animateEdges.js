/**
 * This plugin provides a method to animate a sigma instance by interpolating
 * some node properties. Check the sigma.plugins.animate function doc or the
 * examples/animate.html code sample to know more.
 */
(function() {
  'use strict';

  if (typeof sigma === 'undefined')
    throw 'sigma is not declared';

  sigma.utils.pkg('sigma.plugins');

  var
     _id = 0
    ,easing = sigma.utils.easings.quadraticInOut
    ,_cache = {}
    ,durationDefault = 500
  ;

  /**
   * This function will animate some specified node properties. It will
   * basically call requestAnimationFrame, interpolate the values and call the
   * refresh method during a specified duration.
   *
   * Recognized parameters:
   * **********************
   * Here is the exhaustive list of every accepted parameters in the settings
   * object:
   *
   *   {?array}             edges      An array of node objects or node ids. If
   *                                   not specified, all edges of the graph
   *                                   will be animated.
   *   {?(function|string)} easing     Either the name of an easing in the
   *                                   sigma.utils.easings package or a
   *                                   function. If not specified, the
   *                                   quadraticInOut easing from this package
   *                                   will be used instead.
   *   {?number}            duration   The duration of the animation. If not
   *                                   specified, the "animationsTime" setting
   *                                   value of the sigma instance will be used
   *                                   instead.
   *   {?function}          onComplete Eventually a function to call when the
   *                                   animation is ended.
   *
   * @param  {sigma}   s       The related sigma instance.
   * @param  {object}  animate An hash with the keys being the node properties
   *                           to interpolate, and the values being the related
   *                           target values.
   * @param  {?object} options Eventually an object with options.
   */
  sigma.plugins.animateEdges = function(s, animate, options) {
    var o = options || {},
        id,
        duration = o.duration || s.settings('animationsTime'),
        easing = (typeof o.easing === 'string') ?
          sigma.utils.easings[o.easing] :
          (typeof o.easing === 'function') ? o.easing : sigma.utils.easings.quadraticInOut,
        start = sigma.utils.dateNow(),
        edges,
        k, c, line
        ,edgeCount = 0, drawEdges
        ,curX1, curY1, curX2, curY2, curSize
        ,toX1, toY1, toX2, toY2, toSize
        ,newX1, newX2, newY1, newY2, newSize
        ,source, target
        ,edgeLine, edge
    ;

    if (o.edges && o.edges.length) {
      if (typeof o.edges[0] === 'object')
        edges = o.edges;
      else
        edges = s.graph.edges(o.edges); // argument is an array of IDs
    } else {
      edges = s.graph.edges();
    }
    drawEdges = edges.slice([]);
    
    s.animations = s.animations || Object.create({});
    sigma.plugins.kill(s);

    // Do not refresh edgequadtree during drag:
    for (k in s.cameras) {
      c = s.cameras[k];
      c.edgequadtree._enabled = false;
    }

    function step(timestamp) {
      var
         p = (sigma.utils.dateNow() - start) / duration
        ,k ,c
        ,renderer
      ;

      if (p >= 1) {
        s.graph.edges(edge.id).p = (edge.hiding) ? 0: 1;

        for (k in s.cameras) {
          c = s.cameras[k];
          c.edgequadtree._enabled = true;
        }
        s.refresh();

        edge = drawEdges.shift();
        if(edge) {
          start = sigma.utils.dateNow();
          step();
        } else {
          if (typeof o.onComplete === 'function') {
            o.onComplete();
          }
        }
      } else {
        p = easing(p);

        s.graph.edges(edge.id).color = edge.color;
        if(
          (edge.hiding && s.graph.edges(edge.id).p > 0) ||
          (!edge.hiding && s.graph.edges(edge.id).p < 1)
        ) {
          s.graph.edges(edge.id).p = ((edge.hiding) ? 1 - p : p);

          s.refresh();
          s.animations[id] = requestAnimationFrame(step);
        } else if ( edge.size !== s.graph.edges(edge.id).size ) {

          if(s.graph.edges(edge.id).size < edge.size) {
            s.graph.edges(edge.id).size += (edge.size - s.graph.edges(edge.id).size) * p;
          } else {
            s.graph.edges(edge.id).size = s.graph.edges(edge.id).size - (s.graph.edges(edge.id).size * p);
          }

          s.refresh();
          s.animations[id] = requestAnimationFrame(step);
        } else {
          edge = drawEdges.shift();
          if(edge) {
            start = sigma.utils.dateNow();
            step();
          }
        }

        s.refresh();
      }
    }

    if(edges.length > 0) {
      edges.forEach(ee => {
        if(!s.graph.edges().map(e => { return e.id }).some(f => { return ee.id === f })) {
          ee.p = 0;
          ee.hiding = !!ee.hiding;

          s.graph.addEdge({
            id: ee.id, label: ee.id,
            source: s.graph.nodes().find(n => n._id == ee.source).id,
            target: s.graph.nodes().find(n => n._id == ee.target).id,
            size: ee.size,
            color: ee.color,
            type: "animate",
            p: ee.p,
            hiding: ee.hiding
          });
          s.refresh();
        }
      });

      edge = drawEdges.shift();
      step();
    }
  };

  sigma.plugins.kill = function(s) {
    for (var k in (s.animations || {})) {
      cancelAnimationFrame(s.animations[k]);
      delete s.animations[k];
    }

    // Allow to refresh edgequadtree:
    var k, c;
    for (k in s.cameras) {
      c = s.cameras[k];
      c.edgequadtree._enabled = true;
    }
  };
}).call(window);
